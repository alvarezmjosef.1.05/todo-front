import axios from "axios";

export const getTasks = () => {
    return (dispatch) => {
        axios.get("http://localhost:3001/tasks")
        .then(res => {
            dispatch({
                type: "get_tasks",
                payload: res.data
            })
        })
        .catch(error => {
            console.error(error);
        })
    }
}

export const postTask = (title, description) => {
    return (dispatch) => {
        axios.post("http://localhost:3001/tasks", {title, description})
        .then(res => {
            dispatch({
                type: "post_data",
                payload: res.data
            })
        })
    }
}

export const deleteTask = (id) => {
    return (dispatch) => {
        axios.delete("http://localhost:3001/tasks", {data: {id}})
        .then(res => {
            dispatch({
                type: "delete_data",
                payload: res.data
            })
        })
    }
}

export const updateTask = (id, newTitle, newDescription, newDone) => {
    return (dispatch) => {
        axios.put("http://localhost:3001/tasks", {id , newTitle, newDescription, newDone})
        .then(res => {
            dispatch({
                type: "update_data",
                payload: res.data
            })
        })
    }
}