import React, { useState, useEffect } from 'react'
import { Checkbox, Button, Icon, Confirm, Input } from "semantic-ui-react";

import { connect } from "react-redux";
import { getTasks, deleteTask, updateTask } from "../todo/actions";

function Task(props) {
	const [editing, setEditing] = useState(false);
	const [confirm, setConfirm] = useState(false);
	const [done, setDone] = useState(props.done);
	const [title, setTitle] = useState(props.title);
	const [description, setDescription] = useState(props.description);

	const className = done ? "task task-done" : "task"

	useEffect(() => {
		props.getTasks()
	})

	const handleDelete = () => {
		setConfirm(true);
	}

	const handleSave = async () => {
		await props.updateTask(props.id, title, description, false)
		setEditing(false);
	}

	const handleEdit = () => {
		setEditing(true);
	}

	const handleConfirm = async () => {
		setConfirm(false);
		await props.deleteTask(props.id);
	}

	const handleCheckbox = async (e, {checked}) => {
		setDone(checked);
		await props.updateTask(props.id, title, description, checked)
	}

	return (
		<div className={className}>
			<Checkbox  defaultChecked={props.done} onChange={handleCheckbox}/>
			{
				(editing)
				? (<>
					<Input value={title} onChange={(e) => setTitle(e.target.value)}/>
					<Input value={description} onChange={(e) => setDescription(e.target.value)}/>
					<Button className="task-button" color="green" compact size="mini" onClick={handleSave}><Icon name="save"/></Button>
				</>
				)
				: (<>
					<h3>{title}</h3>
					<p>{description}</p>
					<Button className="task-button" primary compact size="mini" onClick={handleEdit}><Icon name="edit"/></Button>
				</>
				)
			}
			<Button className="task-button" negative compact size="mini" onClick={handleDelete}><Icon name="delete"/></Button>

			<Confirm 
				content={`¿Seguro que desea eliminar la tarea "${props.title}"?`}
				cancelButton="No"
				confirmButton="Sí"
				open={confirm}
				onConfirm={handleConfirm}
				onCancel={() => setConfirm(false)}
			/>
		</div>
	)
}

const mapDispatchToProps = (dispatch) => {
	return {
		getTasks: () => dispatch(getTasks()),
		deleteTask: (id) => dispatch(deleteTask(id)),
		updateTask: (id, newTitle, newDescription, checked) => dispatch(updateTask(id, newTitle, newDescription, checked))
	}
}

export default connect(null, mapDispatchToProps)(Task);