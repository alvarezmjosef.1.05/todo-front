import { useState, useEffect } from "react";

import './App.css';
import { Button, Form, Grid, Input, TextArea } from "semantic-ui-react";
import Task from "./components/Task";

import { connect } from "react-redux";
import { getTasks, postTask } from "./todo/actions";

const App = (props) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");


  useEffect(() => {
    props.getTasks();
  }, []);

  const handleTaskSubmit = async () => {
    await props.postTask(title, description);
    setTitle("");
    setDescription("");
    await props.getTasks();
  }

  return (
    <div className="content">
      <Grid columns="2">
        <Grid.Column width="4">
          <Form className="add-task-form">
            <Input fluid value={title} placeholder="Título" onChange={(e) => setTitle(e.target.value)}/>
            <TextArea value={description} rows={2} placeholder="Descripción" onChange={(e) => setDescription(e.target.value)}/>
            <Button primary fluid onClick={handleTaskSubmit}>Guardar</Button>
          </Form>
        </Grid.Column>
        <Grid.Column width="12">
          {props.tasks.map(item => <Task key={item._id} id={item._id} title={item.title} description={item.description} done={item.done}/>)}
        </Grid.Column>
      </Grid>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks,
    updatedTask: state.updatedTask,
    deletedTask: state.deletedTask
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTasks: () => dispatch(getTasks()),
    postTask: (title, description) => dispatch(postTask(title, description))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
