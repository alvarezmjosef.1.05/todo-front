import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import "semantic-ui-css/semantic.min.css"

import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import todoReducer from "./todo/reducer";

const store = createStore(todoReducer, applyMiddleware(thunk))

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);